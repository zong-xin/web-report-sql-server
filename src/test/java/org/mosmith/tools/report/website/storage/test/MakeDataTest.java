package org.mosmith.tools.report.website.storage.test;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MakeDataTest {
    
    static int count=0;
    
    public static void main(String[] args) throws Exception {
        String driverClass="com.mysql.jdbc.Driver";
        String jdbcUrl="jdbc:mysql://localhost:3306/web_report_saas??characterEncoding=utf8";
        String user="root";
        String password="123456";
        
        Class.forName(driverClass);
        Connection connection=null;
        Statement statement=null;
        ResultSet resultSet=null;
        try {
            connection=DriverManager.getConnection(jdbcUrl, user, password);
            statement=connection.createStatement();
            
            for(int i=0;i<20000;i++) {
                String id=createId("user");
                String name="user" + i;
                String email=name+"@xmreport.com";
                String pwd="123456";
                String apiKey=name;
                
                String sql="insert into t_user values('%s', '%s', '%s', '%s', '%s')";
                sql=String.format(sql, id, name, email, pwd, apiKey);
                statement.execute(sql);
            }
        } finally {
            close(resultSet);
            close(statement);
            close(connection);
        }
    }
    

    public static String createId(String prefix) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        sb.append('-');
        sb.append(String.format("%012x", System.currentTimeMillis()));
        sb.append('-');
        sb.append(String.format("%04d", count++));
        sb.append('-');
        sb.append(String.format("%04d", (int)(Math.random()*9999)));
        
        return sb.toString();
    }
    

    private static void close(Object object) {
        try {
            if(object instanceof Connection) {
                ((Connection)object).close();
            }
            if(object instanceof Statement) {
                ((Statement)object).close();
            }
            if(object instanceof ResultSet) {
                ((ResultSet)object).close();
            }
            if(object instanceof Closeable) {
                ((Closeable)object).close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
